<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
            [
             'title' => 'home work',
            'user_id'=>'1',
            'status'=>'0',
        ],
        [
            'title' => 'home work2',
           'user_id'=>'2',
           'status'=>'0',
       ],
       [
        'title' => 'home work3',
       'user_id'=>'1',
       'status'=>'0',
        ],
        
        ]);
    }
}
