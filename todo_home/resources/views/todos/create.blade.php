@extends('layouts.app')

@section('content')

<h1>Create a new todo</h1>
<form method='post' action="{{action('TodoController@store')}}">
    {{csrf_field()}}

    <div class="form-group">
    <h2>Add a todo</h2>
        <label for ="title">title </label>
        <input type="text" class= "form-control" name="title">
        </div>
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class = "form-group">
        <input type="submit" class="form-control" name="submit" value="save">
    </div>
</form>
@endsection