@extends('layouts.app')

@section('content')

<!DOCTYPE html>
<html>
@if (Request::is('tasks')) 
<h2><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">All Tasks</a></h2>
@endif
<h1> this is a task list</h1>
    <head>
    </head>
    <body>
    <ul>
   @foreach($tasks as $task)
     
     <table>
     <tr>
     @can('admin')


     <th>
     @if ($task->status)
           <button id ="" value="1"> Done!</button>
       @else
           <button style="text-decoration: underline" id ="{{$task->id}}" value="0"> Mark as done</button>
       @endif
       @endcan
</th>
    <th>
     {{$task->title}}  </th> <th><a href="{{route('tasks.edit',$task->id)}}">edit</a> </th>
    @can('admin') <th> <form method = 'post' action="{{action('TaskController@destroy', $task->id)}}">
       @csrf
       @method('DELETE')
         
               <input type ="submit"  name="submit" value ="Delete task">
           
        </form></th>@endcan
   
  </tr>
    </table>
          @endforeach
          </ul>
   </body>
   <a href="{{route('tasks.create')}}">Create a new task</a>
   <script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                      console.log( errorThrown );
                   }
               });               
              location.reload();
           });
       });
   </script>

</html>
@endsection