@extends('layouts.app')

@section('content')
<h1>Update a task</h1>
<form method='post' action="{{action('TaskController@update',$task->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
        <label for ="title">  to task</label>
        <input type="text" class= "form-control" name="title" value="{{$task->title}}">
    </div>
    
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>


@endsection